import { Component, OnInit } from '@angular/core';
import {Contact} from "../models/contact";
import {ContactsService} from "../services/contacts.service";
import {AlertController} from "@ionic/angular";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {

  public myContacts: Contact[];

  constructor(public contacts: ContactsService,
              public alertCtrl: AlertController) { }

  ngOnInit() {
    console.log(' ngOninit ContactsPage');
    this.listContacts();

  }

  listContacts(){
    console.log('[ContactsPage] listContacts()');
    this.myContacts = this.contacts.listContacts().sort();
  }

 /* removeContact(contact:Contact){
    console.log('[ContactsPage] removeContact');

    this.alertCtrl.create({
      header:"Remove contact",
      message: "Are your sure ?",
      buttons: [
          {
            text:'Cancel',
            role:'cancel',
            handler: ()=> {console.log('Cancel clicked');}
          },
        {
          text:'Accept',
          handler: () => {
            this.contacts.removeContact(contact);
            this.listContacts();
          }
        }
      ]
    }).then((alert) => alert.present());
  }*/


  async removeContact(contact:Contact){
    console.log('[ContactsPage] removeContact');

    const alert = await this.alertCtrl.create({
      header: 'Remove contact',
      message: 'Are you sure?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: ()=> {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Accept',
          handler: () => {
            this.contacts.removeContact(contact);
            this.listContacts();
          }
        }
      ]
    });
    await alert.present();
  }


}
